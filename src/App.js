import './App.css';
import Restaurants from "./restaurants.json"
import React, { useState, useEffect } from "react";
import Cuisine from "./Cuisine";
import Restaurant from "./Restaurant";

function App() {

    const [cuisines, setCuisines] = useState([]);
    const [selectedCousines, setSelectedCousines] = useState([]);
    const [filteredRestaurants, setFilteredRestaurants] = useState([]);
    const [vegan, setVegan] = useState(false);
    const [dogFriendly, setDogFriendly] = useState(false);

    useEffect(() => {
        Restaurants.forEach(res => {
            res.cuisine.forEach(cuis => {
                if (!cuisines.find(cuisine => cuis === cuisine)) {
                    setCuisines([...cuisines, cuis])
                }
            })
        });
    }, [cuisines]);

    useEffect(() => {
        let selectedRestaurants = []

        let everythingExist = (arr, target) => target.every(v => arr.includes(v));

        Restaurants.forEach(restaurant => {
            if (vegan && !restaurant['vegan-options']) {
                return;
            }
            if (dogFriendly && !restaurant['dog-friendly']) {
                return;
            }
            if (everythingExist(restaurant.cuisine, selectedCousines)) {
                selectedRestaurants.push(restaurant)
            }
        });

        setFilteredRestaurants(selectedRestaurants);
    }, [selectedCousines, dogFriendly, vegan]);

    const addCousine = (selectedCousine) => {
        if (!selectedCousines.includes(selectedCousine)) {
            setSelectedCousines([...selectedCousines, selectedCousine]);
        } else {
            const updatedCousines = selectedCousines.filter(e => e !== selectedCousine);
            setSelectedCousines(updatedCousines);
        }
    }

    const clearFilter = () => {
        setSelectedCousines([]);
        setDogFriendly(false);
        setVegan(false);
    }

    const filterDogFriendly = () => {
        setDogFriendly(!dogFriendly);
    }

    const filterVegan = () => {
        setVegan(!vegan);
    }

    return (
        <div className="App">
            {cuisines.map(cuisine => {
                return <Cuisine key={cuisine} cuisine={cuisine} addCousine={addCousine} />
            })}
            <hr />
            {filteredRestaurants.map(restaurant =>
                <Restaurant key={restaurant.name} restaurant={restaurant} />
            )}
            <hr />
            <span className="button button-clear pointer" onClick={clearFilter}>Clear Filter</span>
            <span className="button pointer" onClick={filterDogFriendly}>Dog Friendly</span>
            <span className="button pointer" onClick={filterVegan}>Vegan Options</span>

        </div >
    );
}



export default App;
