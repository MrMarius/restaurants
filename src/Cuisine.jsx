import React from 'react';

export default function Cuisine(props) {

    const addCousine = () => {
        props.addCousine(props.cuisine);
    }

    return (
        <div className="pointer">
            <input onClick={() => addCousine()} type="checkbox" id={props.cuisine}/>
            <label className="pointer" htmlFor={props.cuisine}>{props.cuisine}</label><br />
        </div>
    );
}