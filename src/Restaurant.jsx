import React from 'react';

export default function Restaurant(props) {
    const { name, cuisine } = props.restaurant;
    const dogFriendly = props.restaurant['dog-friendly'];
    const vegan = props.restaurant['vegan-options'];
    return (
        <div className="restaurant">
            <strong>{props.restaurant.name}</strong> <br />( Cuisines: {props.restaurant.cuisine.map(c => <span key={c}>{c},</span>)})
            <span className="restaurant-options">{dogFriendly && 'Dog Friendly'}</span>
            <span className="restaurant-options">{vegan && 'Vegan Options'}</span>
        </div>
    );
}