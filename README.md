# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).


Use the following commands to get the required dependencies and start the project
```
# Install dependencies
yarn

# If you are using npm then
npm install

# Run the project locally via
yarn start

# or if youre using npm
npm start
```

Now you should be able to visit `localhost:3000` for the demo